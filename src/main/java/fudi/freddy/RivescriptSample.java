package fudi.freddy;

import com.rivescript.ConcatMode;
import com.rivescript.Config;
import com.rivescript.RiveScript;
import com.rivescript.session.SessionManager;

import java.util.Map;

/**
 * Created by romelldominguez on 12/14/17.
 */
public class RivescriptSample {

    public static void main(String args[]) {
        // RiveScript bot = new RiveScript();

        RiveScript bot = new RiveScript(Config.utf8());

        // bot.loadDirectory("./replies");

        bot.loadFile("SANNA.rive");

        bot.sortReplies();

        String reply = bot.reply("user", "hola chatbot");

        System.out.println(reply);
    }

    private static void configurations() {
        SessionManager sessionManager = null;
        Map<String, String> errors = null;
        RiveScript bot = new RiveScript(Config.newBuilder().throwExceptions(false) // Whether exception throwing is
                                                                                   // enabled
                .strict(true) // Whether strict syntax checking is enabled
                .utf8(false) // Whether UTF-8 mode is enabled
                .unicodePunctuation("[.,!?;:]") // The unicode punctuation pattern
                .forceCase(false) // Whether forcing triggers to lowercase is enabled
                .concat(ConcatMode.NONE) // The concat mode
                .depth(50) // The recursion depth limit
                .sessionManager(sessionManager) // The session manager for user variables
                .errorMessages(errors) // Map of custom error messages
                .build());
    }
}
